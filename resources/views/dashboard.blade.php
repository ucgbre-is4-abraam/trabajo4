<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Pagina de Incio') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <!-- Contenido de la vista de bienvenida -->
                    <section class="section">
                        <div class="container">
                            <h1 class="title has-text-centered mb-4">Bienvenida</h1>
                            <h2 class="subtitle has-text-centered mb-4">Alumno: Aldo Abraam Recalde</h2>
                            <h2 class="subtitle has-text-centered mb-6">Nombre del Proyecto: Relación entre Empresas</h2>

                            <div class="columns is-centered mb-6">
                                <div class="column is-three-quarters">
                                    <figure class="image is-430x430">
                                        <img src="{{ asset('images/DER.png') }}" alt="Imagen por defecto">
                                    </figure>
                                </div>
                            </div>

                            <ul class="menu-list mb-10">
                                <li>
                                    <div class="box">
                                        <a style="color: blue" href="./admin/empresas/listar" class="button is-primary is-large is-fullwidth mb-4">
                                            Listas de empresas
                                            
                                        </a>
                                        
                                    </div>
                                    
                                </li>
                                <li>
                                    <div class="box">
                                        <a style="color: green" href="./admin/rubros/listar" class="button is-primary is-large is-fullwidth mb-4">
                                            Listas de Rubros
                                        </a>
                                    </div>
                                </li>
                                <li>
                                    <div class="box">
                                        <a style="color: brown" href="./admin/rubros_por_empresas/listar" class="button is-primary is-large is-fullwidth">
                                            Listas de Rubros por Empresas
                                        </a>
                                    </div>
                                    <br>
                                    <br>
                                </li>
                            </ul>

                            <div class="has-text-centered">
                                @auth
                                    <!-- Si el usuario está autenticado, muestra un enlace para cerrar sesión -->
                                    <a style="color: red" href="{{ route('logout') }}" class="button is-danger"
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        Cerrar sesión
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                @else
                                    <!-- Si el usuario no está autenticado, muestra enlaces para iniciar sesión y registrar -->
                                    <a href="{{ route('login') }}" class="button is-success">Iniciar sesión</a>
                                    <a href="{{ route('register') }}" class="button is-info">Registrarse</a>
                                @endauth
                                <br>
                                <br>
                                <a href="{{ url('/') }}" class="button is-link is-centered">Volver a Inicio</a>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
