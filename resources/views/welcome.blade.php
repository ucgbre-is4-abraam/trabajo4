<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bienvenida</title>
    <link rel="stylesheet" href="{{ asset('css/bulma9.4_css_bulma.min.css') }}">
    <style>
        /* Personalización de estilos */
        .button-login {
            background-color: #4CAF50; /* Verde */
            border: none;
            color: white;
            padding: 10px 20px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            transition-duration: 0.4s;
            cursor: pointer;
            border-radius: 8px;
        }

        .button-login:hover {
            background-color: #45a049; /* Verde más oscuro al pasar el cursor */
        }

        .button-logout {
            background-color: #f44336; /* Rojo */
            border: none;
            color: white;
            padding: 10px 20px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            transition-duration: 0.4s;
            cursor: pointer;
            border-radius: 8px;
        }

        .button-logout:hover {
            background-color: #d32f2f; /* Rojo más oscuro al pasar el cursor */
        }
    </style>
</head>
<body>
    <section class="section">
        <div class="container">
            
            <!-- Encabezado -->
            <div class="header">
                <div class="columns is-mobile is-vcentered">
                    <div class="column">
                        <h1 class="title has-text-centered">Bienvenida</h1>
                        <h2 class="subtitle has-text-centered">Alumno: Aldo Abraam Recalde</h2>
                        <h2 class="subtitle has-text-centered">Nombre del Proyecto: Relación entre Empresas</h2>
                    </div>
                    <div class="column is-narrow">
                        @auth
                            <!-- Botón de Cerrar sesión -->
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
                                <button class="button-logout" type="submit">Cerrar sesión</button>
                            </form>
                        @else
                            <!-- Botón de Iniciar sesión -->
                            <a class="button-login" href="{{ route('login') }}">Iniciar sesión</a>
                            <!-- Botón de Iniciar sesión con cuenta de GOOGLE -->
                            
                            <!-- Botón de Registrarse -->
                            <a class="button-login" href="{{ route('register') }}">Registrarse</a>
                        @endauth
                        <a href="/google-auth/redirect">Iniciar Cuenta de Google</a>
                    </div>
                </div>
            </div>

            <!-- Resto del contenido principal -->
            <div class="columns is-centered">
                <div class="column is-three-quarters">
                    <figure class="image is-430x430">
                        <img src="{{ asset('images/DER.png') }}" alt="Imagen por defecto">
                    </figure>
                </div>
            </div>

            <ul class="menu-list">
                <li><a href="./admin/empresas/listar">Listas de empresas</a></li>
                <li><a href="./admin/rubros/listar">Listas de Rubros</a></li>
                <li><a href="./admin/rubros_por_empresas/listar">Listas de Rubros por Empresas</a></li>
            </ul>
        </div>
    </section>
</body>
</html>