@auth
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/bulma9.4_css_bulma.min.css') }}">
    <title>Empresas</title>
</head>
<body>
    <section class="section">
        <div class="container">
            <!-- Aquí se muestra el nombre de usuario -->
            <div class="user-info" style="text-align: right;">
                Usuario: {{ auth()->user()->name }}
            </div>
            <h1 class="title">Lista de Empresas</h1>

            <!-- Formulario de búsqueda -->
            <form action="{{ route('empresas.listar') }}" method="GET">
                <div class="field is-grouped">
                    <div class="control">
                        <input class="input" type="text" name="search" value="{{ $buscar }}" placeholder="Buscar por nombre o razón social">
                    </div>
                    <div class="control">
                        <label class="checkbox">
                            <input type="checkbox" name="sin_representantes" id="sin_representantes" {{ $sinRepresentantes ? 'checked' : '' }}>
                            Buscar empresas sin representantes
                        </label>
                    </div>
                    <div class="control">
                        <button class="button is-info" type="submit">Buscar</button>
                    </div>
                </div>
            </form>

            <!-- Tabla de Empresas -->
            <table class="table is-fullwidth is-hoverable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Sitio Web</th>
                        <th>Razón Social</th>
                        <th>RUC</th>
                        <th>Fecha de Fundación</th>
                        <th>Número de Representantes</th>
                        <th>Número de Rubros</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($empresas as $empresa)
                    <tr>
                        <td>{{ $empresa->id }}</td>
                        <td>{{ $empresa->nombre }}</td>
                        <td>
                            @if($empresa->sitio_web)
                            <a href="{{ $empresa->sitio_web }}" target="_blank">{{ $empresa->sitio_web }}</a>
                            @else
                            Sin sitio web
                            @endif
                        </td>
                        <td>{{ $empresa->razon_social }}</td>
                        <td>{{ $empresa->ruc }}</td>
                        <td>{{ $empresa->fundacion }}</td>
                        <td>{{ $empresa->representantes->count() }}</td>
                        <td>{{ $empresa->rubros->count() }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <a href="{{ url('/dashboard') }}" class="button is-link is-centered">Volver al Menú</a>
        </div>
    </section>
</body>
</html>
@else
    <!-- Si el usuario no está autenticado, redirigirlo a la página de inicio de sesión -->
    <script>window.location = "{{ route('login') }}";</script>
@endauth