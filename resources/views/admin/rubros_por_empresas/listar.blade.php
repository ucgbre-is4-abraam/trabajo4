@auth
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rubros por Empresas</title>
    <link rel="stylesheet" href="{{ asset('css/bulma9.4_css_bulma.min.css') }}">
  
</head>

<body>
    <section class="section">
        <div class="container">
            <!-- Aquí se muestra el nombre de usuario -->
            <div class="user-info" style="text-align: right;">
                Usuario: {{ auth()->user()->name }}
            </div>
            <h1 class="title">Registros de Rubros por Empresas</h1>
            @if ($rubrosPorEmpresas->isEmpty())
                <p style="color: red">No Existen Registros Disponibles.</p>
                <br>
                <br>
                <a href="{{ url('/') }}" class="button is-link is-centered">Volver al Inicio</a>
            @else
                @php
                    $groupedByRubro = $rubrosPorEmpresas->groupBy('rubro.nombre');
                @endphp

                <table class="table is-fullwidth">
                    <thead>
                        <tr>
                            <th>Rubro</th>
                            <th>Nombre</th>
                            <th>Activo</th>
                            <th>Fecha Desde</th>
                            <th>Fecha Hasta</th>
                            <th>Número de Representantes</th> 
                       
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($groupedByRubro as $rubroNombre => $registros)
                            <tr class="toggle-row" data-toggle="row-{{ $loop->index }}">
                                <td>
                                    <a href="javascript:void(0);" class="toggle-link">{{ $rubroNombre }}</a>
                                </td>
                                <td colspan="4"></td> <!-- Ajuste por la  columna -->
                            </tr>
                            @foreach ($registros as $registro)
                            <tr class="collapse-row row-{{ $loop->parent->index }}" style="display: none;">
                                <td></td>
                                <td>{{ $registro->empresa->nombre }}</td>
                                <td class="{{ $registro->activo ? 'has-background-success' : 'has-background-danger' }}">
                                    {{ $registro->activo ? 'Sí' : 'No' }}
                                </td>
                                <td>{{ $registro->fecha_desde }}</td>
                                <td>{{ $registro->fecha_hasta }}</td>
                                <td>{{ $registro->empresa->representantes->count() }}</td> 
                            </tr>
                        @endforeach
                        @endforeach
                    </tbody>
                </table>
                <a href="{{ url('/dashboard') }}" class="button is-link is-centered">Volver al Menú</a>

            @endif
        </div>
    </section>

    <script>
        const toggleLinks = document.querySelectorAll('.toggle-link');

        toggleLinks.forEach((toggleLink) => {
            toggleLink.addEventListener('click', () => {
                const toggleRow = toggleLink.parentNode.parentNode;
                const rowClass = toggleRow.dataset.toggle;
                const rowsToToggle = document.querySelectorAll(`.${rowClass}`);

                rowsToToggle.forEach((row) => {
                    row.style.display = (row.style.display === 'none') ? 'table-row' : 'none';
                });
            });
        });
    </script>
</body>

</html>

@else
    <!-- Si el usuario no está autenticado, redirigirlo a la página de inicio de sesión -->
    <script>window.location = "{{ route('login') }}";</script>
@endauth
