@auth
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/bulma9.4_css_bulma.min.css') }}">
    <title>Rubros</title>
</head>
<body>
    <section class="section">
        <div class="container">
            <!-- Aquí se muestra el nombre de usuario -->
            <div class="user-info" style="text-align: right;">
                Usuario: {{ auth()->user()->name }}
            </div>
            <h1 class="title">Lista de Rubros</h1>

            <form action="{{ route('rubros.listar') }}" method="GET" class="field is-grouped">
                <div class="control">
                    <input class="input" type="text" name="nombre" placeholder="Buscar por nombre" value="{{ $nombre ?? '' }}">
                </div>
                
                <div class="control">
                   
                    <button class="button is-info" type="submit">Buscar</button>
                </div>
                <div class="control">
                    <label for="activo" class="label">Activo:</label>
                    <div class="select">
                        <select name="activo">
                            <option value="">Todos</option>
                            <option value="1" {{ request('activo') === '1' ? 'selected' : '' }}>Activo</option>
                            <option value="0" {{ request('activo') === '0' ? 'selected' : '' }}>Inactivo</option>
                        </select>
                    </div>
                </div>
                <div class="control">
                    <label for="sin_empresas" class="label">Sin Empresas:</label>
                    <div class="select">
                        <select name="sin_empresas">
                            <option value="">Todos</option>
                            <option value="si" {{ $sinEmpresas === 'si' ? 'selected' : '' }}>Sí</option>
                            <br>
                        </select>
                    </div>
                </div>
            </form>
            <table class="table is-fullwidth is-hoverable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Activo</th>
                        <th>Número de Empresas</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rubros as $rubro)
                    <tr>
                        <td>{{ $rubro->id }}</td>
                        <td>{{ $rubro->nombre }}</td>
                        <td>{{ $rubro->activo ? 'Sí' : 'No' }}</td>
                        <td>{{ $rubro->empresas->count() }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <br>
            <a href="{{ url('/dashboard') }}" class="button is-link is-centered">Volver Volver al Menú</a>
        </div>
    </section>
</body>
</html>

@else
    <!-- Si el usuario no está autenticado, redirigirlo a la página de inicio de sesión -->
    <script>window.location = "{{ route('login') }}";</script>
@endauth