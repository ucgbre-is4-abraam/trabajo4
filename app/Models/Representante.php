<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Representante extends Model
{
    use HasFactory;

    protected $table = 'representantes';
    protected $fillable = ['empresa_id','nombre_completo', 'cargo', 'email', 'telefonos'];


//relacion-metodo en singular (recomendacion)
    // Relacion inversa de uno a muchos, muchos a uno
    // Uno o muchos representantes pertenece a una empresa

    public function empresa(): BelongsTo
    {
//1er argumento: la clase del modelo padre (empresa) - estamos dentro del modelo rep.
        //2do argumento: llave foranea (fk) en este caso empresa_id - ver DER
        //3er argumento: llave primaria pk del modelo actual
        return $this->belongsTo(Empresa::class, 'empresa_id', 'id');
    }




}

