<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rubro_por_Empresa extends Model
{
    use HasFactory;
    protected $table= 'rubros_por_empresas';
    protected $fillable = ['empresa_id', 'rubro_id', 'activo', 'fecha_desde', 'fecha_hasta'];


/*
    public function rubros()
    {
    return $this->belongsToMany(Rubro::class, 'rubros_por_empresas','empresas_id', 'rubros_id');
    }

    public function empresas()
    {
    return $this->belongsToMany(Empresa::class, 'rubros_por_empresas','rubros_id','empresas_id');
    
    }
*/

    public function empresa()
    {
        return $this->belongsTo(Empresa::class, 'empresa_id');
    }

    public function rubro()
    {
        return $this->belongsTo(Rubro::class, 'rubro_id');
    }

}
