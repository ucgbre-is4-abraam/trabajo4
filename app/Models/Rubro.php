<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rubro extends Model
{
    use HasFactory;
    protected $table = 'rubros';
    protected $fillable = ['nombre', 'activo',];

    //Relacion muchos a muchos
    //Un rubro tiene uno o muchas empresas

    public function empresas()
    {
        //1er argumento: clase del modelo relacionado
        //2do argumento: nombre de la tabla intermedia
        //3er argumento: fk del modelo asociado (mapeado)
        //4to argumento: el fk del otro modelo relacionado
    return $this->belongsToMany(Empresa::class, 'rubros_por_empresas','rubro_id','empresa_id'  );
   
    }

}
