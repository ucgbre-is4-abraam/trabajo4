<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;


class Empresa extends Model
{
    use HasFactory;
    protected $table = 'empresas';
    protected $fillable = ['nombre', 'sitio_web', 'razon_social', 'ruc', 'fundacion'];

    // Relacion de uno a muchos HasMany (Empresa tiene uno o muchos representantes)
    // buena practica defirir el metodo en plural

    public function representantes(): HasMany
    {
        // Primer arguemento: Clase del modelo relacionado
        // 2do argumento: Fk 
        //3er argumento: La PK de este modelo  
        return $this->hasMany(Representante::class, 'empresa_id', 'id');
    }
    //
    //Relacion muchos a muchos
    //Una empresa tiene varios rubros
    public function rubros()
    {
        //1er argumento: clase del modelo relacionado
        //2do argumento: nombre de la tabla intermedia
        //3er argumento: fk del modelo asociado (mapeado)
        //4to argumento: el fk del otro modelo relacionado
        return $this->belongsToMany(Rubro::class, 'rubros_por_empresas','empresa_id', 'rubro_id');
    }
}
