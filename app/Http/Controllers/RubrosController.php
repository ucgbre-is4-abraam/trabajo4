<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Rubro;

class RubrosController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $query = Rubro::query();
    $nombre = $request->input('nombre');
    $sinEmpresas = $request->input('sin_empresas');
    $activo = $request->input('activo'); // Nuevo campo para filtrar por estado activo o inactivo

    if ($nombre) {
        $query->where('nombre', 'ilike', '%' . $nombre . '%');
    }

    if ($sinEmpresas === 'si') {
        $query->whereDoesntHave('empresas');
    }

    if ($activo !== null) {
        $query->where('activo', $activo);
    }

    $rubros = $query->get();

    return view('admin.rubros.listar', compact('rubros', 'nombre', 'sinEmpresas', 'activo'));

    }
    


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
