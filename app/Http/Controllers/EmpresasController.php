<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Empresa;
use Illuminate\Http\Request;

class EmpresasController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $query = Empresa::query();
        $buscar = $request->input('search', ''); // Término de búsqueda

        // Verificar si hay un término de búsqueda y aplicar los filtros
        if ($buscar) {
            // Convertir el término de búsqueda a minúsculas para buscar sin distinguir mayúsculas y minúsculas
            $buscar = strtolower($buscar);

            $query->whereRaw('LOWER(nombre) LIKE ?', ["%{$buscar}%"])
                ->orWhereRaw('LOWER(razon_social) LIKE ?', ["%{$buscar}%"]);
        }

        // Filtrar empresas sin representantes asociados
        $sinRepresentantes = $request->input('sin_representantes', false);
        if ($sinRepresentantes) {
            $query->doesntHave('representantes');
        }

        // Obtener los resultados de la consulta paginados
        $empresas = $query->paginate(10);

        return view('admin.empresas.listar', [
            'empresas' => $empresas,
            'buscar' => $buscar,
            'sinRepresentantes' => $sinRepresentantes,
        ]);
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
