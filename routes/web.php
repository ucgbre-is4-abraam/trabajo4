<?php
use App\Models\User;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmpresasController;
use App\Http\Controllers\Rubros_Por_EmpresasController;
use App\Http\Controllers\RubrosController;
use App\Models\Rubro;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/google-auth/redirect', function () {
    return Socialite::driver('google')->redirect();
});
 
Route::get('/google-auth/callback', function () {
    $user_google = Socialite::driver('google')->user();
    
    // Busca un usuario existente por su correo electrónico
    $user = User::where('email', $user_google->email)->first();

    if (!$user) {
        // Si no se encuentra un usuario existente, crea uno nuevo
        $user = User::create([
            'name' => $user_google->name,
            'email' => $user_google->email,
            'google_id' => $user_google->id,
        ]);
    }

    Auth::login($user);
    return redirect('/dashboard');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});
//Pagina de empresas
Route::get('/admin/empresas/listar', [EmpresasController::class, 'index'])->name('empresas.listar');


Route::get('/admin/rubros/listar', [RubrosController::class, 'index'])->name('rubros.listar');

Route::get('/admin/rubros_por_empresas/listar', [Rubros_Por_EmpresasController::class, 'index'])->name('rubros_por_empresas.listar');


require __DIR__.'/auth.php';

