<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('rubros_por_empresas', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedBigInteger('empresa_id');
            $table->foreign('empresa_id')->references('id')->on('empresas');

            $table->unsignedBigInteger('rubro_id');
            $table->foreign('rubro_id')->references('id')->on('rubros');

            $table->boolean('activo')->default(true)->comment('Campo para el borrado logico del rubro');
            $table->date('fecha_desde')->comment('Fecha de inicio del rubro');
            $table->date('fecha_hasta')->nullable()->comment('fecha de fin del rubro');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rubros_por_empresas');
    }
};
