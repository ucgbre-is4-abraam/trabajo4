<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 120)->comment('Nombre de la Empresa');
            //$table->string('slug', 120)->comment('Nombre de la empresa normalizado');
            $table->string('sitio_web', 200)->nullable()->comment('Sitio web de la empresa');
            $table->string('razon_social', 40)->comment('Razon Social de la Empresa');
            $table->string('ruc', 40)->comment('Ruc de la empresa');
            $table->date('fundacion')->comment('fundacion de la empresa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('empresas');
    }
};
