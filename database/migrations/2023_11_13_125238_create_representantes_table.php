<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('representantes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('empresa_id');
            $table->foreign('empresa_id')
            ->references('id')
            ->on('empresas')
            ->onUpdate('cascade')
            ->onDelete('set null');


            $table->string('nombre_completo', 250)->comment('Nombre del Representante');
           // $table->string('slug', 250)->comment('Nombre del representante normalizado');
            $table->string('cargo', 120)->comment('cargo del representante');
            $table->string('email', 120)->nullable()->comment('correo del representante');
            $table->string('telefonos', 120)->nullable()->comment('telefono del representante');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('representantes');
    }
};
