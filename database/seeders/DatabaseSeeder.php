<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Empresa;
use App\Models\Representante;
use App\Models\Rubro;
use App\Models\Rubro_por_Empresa;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    
    public function run(): void
    {
        //CARGAR DATOS EN LA TABLA RUBROS
        $rubro = new Rubro();
        $rubro->nombre = "Ganaderia";
        $rubro->activo = true;
        $rubro->save();

        $rubro = new Rubro();
        $rubro->nombre = "Agricultura";
        $rubro->activo = true;
        $rubro->save();

        $rubro = new Rubro();
        $rubro->nombre = "Informática";
        $rubro->activo = true;
        $rubro->save();

        $rubro = new Rubro();
        $rubro->nombre = "Contabilidad";
        $rubro->activo = true;
        $rubro->save();

        
        $rubro = new Rubro();
        $rubro->nombre = "Arquitectura";
        $rubro->activo = true;
        $rubro->save();

        //CARGAR DATOS EN LA TABLA EMPRESAS
        $empresa = new Empresa();
        $empresa->nombre = 'ABRAHAM S.A';
        $empresa->sitio_web = 'https://www.abraham.com/';
        $empresa->razon_social= 'ABRAHAM';
        $empresa->ruc='4.681.427-2';
        $empresa->fundacion='13-12-2015';
        $empresa->save();

        
        $empresa = new Empresa();
        $empresa->nombre = 'REBOLLO S.A.';
        $empresa->sitio_web = 'https://www.bca.com.py/';
        $empresa->razon_social= 'REBOLLO';
        $empresa->ruc='5.412.850-5';
        $empresa->fundacion='31-08-2014';
        $empresa->save();

        $empresa = new Empresa();
        $empresa->nombre = 'BERENDT S.R.L';
        $empresa->sitio_web = 'https://smg.legal/';
        $empresa->razon_social= 'BERENDT';
        $empresa->ruc='800.427-5';
        $empresa->fundacion='14-10-2000';
        $empresa->save();

        
        $empresa = new Empresa();
        $empresa->nombre = 'TRIPAR S.A';
        $empresa->sitio_web = 'https://tripar.com.py/';
        $empresa->razon_social= 'Tripera Paraguaya SA';
        $empresa->ruc='80012763-3';
        $empresa->fundacion='07-02-1997';
        $empresa->save();

        $empresa = new Empresa();
        $empresa->nombre = 'LAS TACUARAS';
        $empresa->sitio_web = 'https://nutrihuevos.com.py/';
        $empresa->razon_social= 'Nutri Huevos';
        $empresa->ruc='80010102-5';
        $empresa->fundacion='07-07-1970';
        $empresa->save();

         //CARGAR DATOS EN LA TABLA REPRESENTANTES
         $representante = new Representante();
         $representante->empresa_id=1;
         $representante->nombre_completo='Aldo Abraham Recalde';
         $representante->cargo='Propietario';
         $representante->email='aldo.recalde@abraham.com.py';
         $representante->telefonos='+595 984 554 128';
         $representante->save();
 
         
         $representante = new Representante();
         $representante->empresa_id=2;
         $representante->nombre_completo='Diana Rebollo';
         $representante->cargo='Director';
         $representante->email='diana.rebollo@rebollo.com.py';
         $representante->telefonos='+595 982 848 850';
         $representante->save();
 
 
         $representante = new Representante();
         $representante->empresa_id=3;
         $representante->nombre_completo='Eliseo Berendt';
         $representante->cargo='Director';
         $representante->email='elise.berendt@berendt.com.py';
         $representante->telefonos='+595 981 340 007';
         $representante->save();
 
         $representante = new Representante();
         $representante->empresa_id=4;
         $representante->nombre_completo='Flavio Recalde';
         $representante->cargo='Gerente';
         $representante->email='flavio.recalde@tripar.com.py';
         $representante->telefonos='+595 981 340 250';
         $representante->save();
 
         $representante = new Representante();
         $representante->empresa_id=5;
         $representante->nombre_completo='Alfredo Koh';
         $representante->cargo='Propietario';
         $representante->email='alfredo.koh@lastacuaras.com.py';
         $representante->telefonos='+595 981 007 250';
         $representante->save();

        //CARGAR DATOS EN LA TABLA rubros_por_empresas
        $rubro_por_empresa = new Rubro_por_Empresa();
        $rubro_por_empresa->empresa_id = 1;
        $rubro_por_empresa->rubro_id = 1;
        $rubro_por_empresa->activo = true;
        $rubro_por_empresa->fecha_desde = '2020-04-13';
        $rubro_por_empresa->fecha_hasta = null; 
        $rubro_por_empresa->save();
       
        $rubro_por_empresa = new Rubro_por_Empresa();
        $rubro_por_empresa->empresa_id = 2;
        $rubro_por_empresa->rubro_id = 2;
        $rubro_por_empresa->activo = true;
        $rubro_por_empresa->fecha_desde = '2020-04-13';
        $rubro_por_empresa->fecha_hasta = null; 
        $rubro_por_empresa->save();

        $rubro_por_empresa = new Rubro_por_Empresa();
        $rubro_por_empresa->empresa_id = 3;
        $rubro_por_empresa->rubro_id = 3;
        $rubro_por_empresa->activo = true;
        $rubro_por_empresa->fecha_desde = '2014-07-07';
        $rubro_por_empresa->fecha_hasta = null; 
        $rubro_por_empresa->save();
       
        $rubro_por_empresa = new Rubro_por_Empresa();
        $rubro_por_empresa->empresa_id = 4;
        $rubro_por_empresa->rubro_id = 4;
        $rubro_por_empresa->activo = true;
        $rubro_por_empresa->fecha_desde = '2017-05-25';
        $rubro_por_empresa->fecha_hasta = null; 
        $rubro_por_empresa->save();

        $rubro_por_empresa = new Rubro_por_Empresa();
        $rubro_por_empresa->empresa_id = 5;
        $rubro_por_empresa->rubro_id = 5;
        $rubro_por_empresa->activo = true;
        $rubro_por_empresa->fecha_desde = '2000-02-07';
        $rubro_por_empresa->fecha_hasta = null; 
        $rubro_por_empresa->save();
       
       
       
    }
    }

